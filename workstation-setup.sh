#!/usr/bin/env bash
set -e

OS_FAMILY=$(uname)

YELLOW="\e[33m"
CYAN="\e[36m"
NORMAL="\e[0m"

main(){
    get_system_info
    backup_and_copy_settings
    install_extensions
}

get_system_info(){
    case "$OS_FAMILY" in
        Linux)
            CONFIG_DIR="${HOME}/.config/Code/User/"
            ;;
        Darwin)
            CONFIG_DIR="${HOME}/Library/Application Support/Code/User/"
            ;;
        *)
            echo "OS $OS_FAMILY Not supported at this time.  Plase fork this and put in a PR."
            ;;
    esac
}

backup_and_copy_settings(){
    echo "Backing up system config and copying settings from this directory."
    cp -a "${CONFIG_DIR}/settings.json"{,-"$(date +%F_%H:%M:%S)"} ||
        echo "${YELLOW}No existing config found.${NORMAL}"
    cp ./settings.json "$CONFIG_DIR"
}

install_extensions(){
    while read -r EXTENSION; do
        code --install-extension "$EXTENSION" --force ||
            echo -e "${CYAN}$EXTENSION did not install.${NORMAL}"
    done < extensions.list
}

main
