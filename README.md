# vscode-settings

## Why store this in a repo?

I figured that this was the easiest way to sync my vscode settings and extentions between machines.

## What this does today

* Backups your current settings.json with timestamp
* Installes extensions that are listed in extensions.list

## What this is needing to do in the future

* Ability to uninstall plugins that are no longer in the extensions.list with an ability to create a restore list.
* PowerShell script for windows machines.
* The settings.json and extensions.list should be stored somewhere else (maybe a versioned s3 bucket) so that this can be made more generic for myriad users.
** This idea could be used to create an extension for syncing VSCode configurations for an entire organization.
